<?php

namespace YSquare\CategoryPath\Application\Model;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Database\QueryBuilderFactoryInterface;

class CategoryList extends CategoryList_parent {
    protected $ys_category_path_join_delimiter = ";";
    protected $queryBuilderFactory;

    /**
     * @param bool $blVerbose
     * @param null $sShopID
     * @throws Exception
     */
    public function updateCategoryTree($blVerbose = true, $sShopID = null) {
        parent::updateCategoryTree($blVerbose, $sShopID);

        $this->ys_build_category_path();
    }

    /**
     * @throws ConnectionException
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws \Doctrine\DBAL\Exception
     */
    public function ys_build_category_path() {
        $container = ContainerFactory::getInstance()->getContainer();
        $this->queryBuilderFactory = $container->get(QueryBuilderFactoryInterface::class);

        /** @var QueryBuilder $query */
        $query = $this->queryBuilderFactory->create();
        $connection = $query->getConnection();
        try {
            $connection->beginTransaction();

            $query->update('oxcategories', 'oc')
                ->set('oc.YS_CATEGORYID_PATH', ':idPath')
                ->set('oc.YS_CATEGORY_PATH', ':titlePath')
                ->set('oc.YS_CATEGORY_PATH_1', ':titlePath_1')
                ->set('oc.YS_CATEGORY_PATH_2', ':titlePath_2')
                ->set('oc.YS_CATEGORY_PATH_3', ':titlePath_3')
                ->setParameters([
                    'idPath'      => '',
                    'titlePath'   => '',
                    'titlePath_1' => '',
                    'titlePath_2' => '',
                    'titlePath_3' => '',
                ])
                ->execute();

            # Get all root categories
            $query = $this->queryBuilderFactory->create();
            $results = $query->select('OXID', 'OXTITLE', 'OXTITLE_1', 'OXTITLE_2', 'OXTITLE_3')
                ->from('oxcategories')
                ->where("OXPARENTID = 'oxrootid'")
                ->orderBy('OXSORT')
                ->execute()->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($results as $result) {
                $this->_aUpdateInfo[] = "<b>(Category Path) Processing : " . $result["OXTITLE"] . " </b>(" . $result["OXID"] . ")<br>";
                $oxid = $result["OXID"];
                $this->ys_update_nodes($oxid, $oxid, $result["OXTITLE"], $result["OXTITLE_1"], $result["OXTITLE_2"], $result["OXTITLE_3"]);
            }
            $connection->commit();
        } catch (Exception $exception) {
            $connection->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $oxid
     * @param $idPath
     * @param $titlePath
     * @param $titlePath_1
     * @param $titlePath_2
     * @param $titlePath_3
     * @throws \Doctrine\DBAL\Exception
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    protected function ys_update_nodes($oxid, $idPath, $titlePath, $titlePath_1, $titlePath_2, $titlePath_3) {
        /** @var QueryBuilder $query */
        $query = $this->queryBuilderFactory->create();
        $query->update('oxcategories', 'oc')
            ->set('oc.YS_CATEGORYID_PATH', ':idPath')
            ->set('oc.YS_CATEGORY_PATH', ':titlePath')
            ->set('oc.YS_CATEGORY_PATH_1', ':titlePath_1')
            ->set('oc.YS_CATEGORY_PATH_2', ':titlePath_2')
            ->set('oc.YS_CATEGORY_PATH_3', ':titlePath_3')
            ->where('oc.OXID = :oxid')
            ->setParameters([
                "oxid"        => $oxid,
                "idPath"      => $idPath,
                "titlePath"   => $titlePath,
                "titlePath_1" => $titlePath_1,
                "titlePath_2" => $titlePath_2,
                "titlePath_3" => $titlePath_3
            ])
            ->execute();

        $query = $this->queryBuilderFactory->create();
        $results = $query->select('OXID', 'OXACTIVE', 'OXTITLE', 'OXTITLE_1', 'OXTITLE_2', 'OXTITLE_3')
            ->from('oxcategories')
            ->where("OXPARENTID = :oxid")
            ->orderBy('OXSORT')
            ->setParameters([
                "oxid" => $oxid
            ])
            ->execute()->fetchAll(FetchMode::ASSOCIATIVE);

        $sub_category_ids = [];
        $sub_categories = [];

        foreach ($results as $result) {
            $sub_oxid = $result["OXID"];
            $active = $result["OXACTIVE"];
            $title = $result["OXTITLE"];
            $title_1 = $result["OXTITLE_1"];
            $title_2 = $result["OXTITLE_2"];
            $title_3 = $result["OXTITLE_3"];
            if ($active) {
                $sub_category_ids[] = $sub_oxid;
                $sub_categories[] = $title;
            }
            $tmp_idPath = $idPath . $this->ys_category_path_join_delimiter . $sub_oxid;
            $tmp_titlePath = $titlePath . $this->ys_category_path_join_delimiter . $title;
            $tmp_titlePath_1 = $titlePath_1 . $this->ys_category_path_join_delimiter . $title_1;
            $tmp_titlePath_2 = $titlePath_2 . $this->ys_category_path_join_delimiter . $title_2;
            $tmp_titlePath_3 = $titlePath_3 . $this->ys_category_path_join_delimiter . $title_3;

            $this->ys_update_nodes($sub_oxid, $tmp_idPath, $tmp_titlePath, $tmp_titlePath_1, $tmp_titlePath_2, $tmp_titlePath_3);
        }

        $query = $this->queryBuilderFactory->create();
        $query->update('oxcategories', 'oc')
            ->set('oc.YS_SUB_CATEGORIES', ':subCategories')
            ->set('oc.YS_SUB_CATEGORY_IDS', ':subCategoryIds')
            ->where('oc.OXID = :oxid')
            ->setParameters([
                "oxid"           => $oxid,
                "subCategories"  => implode(";", $sub_categories),
                "subCategoryIds" => implode(";", $sub_category_ids)
            ])
            ->execute();
    }

}
