<?php

namespace YSquare\CategoryPath\Core;

class Events {

    /**
     * Add missing tables if not already existent
     */
    public static function addMissingTablesOnUpdate() {
    }

    /**
     * Add missing field if it activates on old DB
     */
    public static function addMissingFieldsOnUpdate() {
        /*
         * Extending Articles Table
         */
        $aTableFields = [
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_CATEGORY_PATH",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Category path'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_CATEGORY_PATH_1",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Category path'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_CATEGORY_PATH_2",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Category path'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_CATEGORY_PATH_3",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Category path'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_CATEGORYID_PATH",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Category ids path'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_SUB_CATEGORIES",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Sub categories'"
            ],
            [
                'table'     => "oxcategories",
                'fieldname' => "YS_SUB_CATEGORY_IDS",
                'fieldtype' => "varchar(512) NOT NULL DEFAULT '' COMMENT 'Sub category ids'"
            ]
        ];
        self::executeTableFieldAddition($aTableFields);
    }

    public static function executeTableFieldAddition($aFieldsToAdd) {

        $oDbMetaDataHandler = oxNew('oxDbMetaDataHandler');
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        if (is_array($aFieldsToAdd) && is_array($aFieldsToAdd[0])) {

            // This case covers an array with several fields
            foreach ($aFieldsToAdd as $aField) {
                if (!$oDbMetaDataHandler->fieldExists($aField['fieldname'], $aField['table'])) {
                    $oDb->execute(
                        "ALTER TABLE `" . $aField['table']
                        . "` ADD `" . $aField['fieldname'] . "` " . $aField['fieldtype'] . ";"
                    );
                }
            }
        } elseif (is_array($aFieldsToAdd) && isset($aFieldsToAdd['table'])) {

            // This case covers one field given alone
            if (!$oDbMetaDataHandler->fieldExists($aFieldsToAdd['fieldname'], $aFieldsToAdd['table'])) {
                $oDb->execute(
                    "ALTER TABLE `" . $aFieldsToAdd['table']
                    . "` ADD `" . $aFieldsToAdd['fieldname'] . "` " . $aFieldsToAdd['fieldtype'] . ";"
                );
            }
        }
    }

    /**
     * Execute action on activate event
     */
    public static function onActivate() {
        self::addMissingTablesOnUpdate();
        self::addMissingFieldsOnUpdate();
    }

    /**
     * Execute action on deactivate event
     *
     * @return null
     */
    public static function onDeactivate() {

    }
}