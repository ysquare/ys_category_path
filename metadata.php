<?php
/**
 * This Software is the property of Y-SQUARE GbR
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * is a violation of the license agreement and will be prosecuted
 * by civil and criminal law.
 *
 * Inhaber: Daniela Baumgartner & Thorsten Schneider
 * Alle Rechte vorbehalten
 *
 * @version 1.0.0
 * @author Y-SQUARE GbR <info@y-square.de>
 * @copyright (C) 2020, Y-SQUARE GbR
 * @see http://www.y-square.de
 */

/**
 * Y-SQUARE Prequel Text
 */
$ys_prequel = "<style>.listitemfloating .logo {display:inline-block; width:15px; height:15px; line-height: 16px; text-align: center; background-color: #000; color:#FFF;} h1 .logo {display:inline-block; width:31px; height:31px; line-height: 31px; text-align: center; background-color: #000; color:#FFF;}</style><span class=\"logo\">Y</span> <b>[Y-SQUARE]</b> ";

/**
 * Metadata version
 */
$sMetadataVersion = '2.1';

/**
 * Module information
 */
$aModule = [
    'id'          => 'ys_category_path',
    'title'       => $ys_prequel . 'Category path',
    'description' => [
        'de' => 'Speichert für Kategorien den Pfad zur root Kategory',
        'en' => 'Saves path to root category for categories',
    ],
    'thumbnail'   => 'logo.png',
    'version'     => '1.0.1',
    'author'      => 'Y-SQUARE GbR',
    'url'         => 'https://www.y-square.de',
    'email'       => 'info@y-square.de',
    'extend'      => [
        \OxidEsales\Eshop\Application\Model\CategoryList::class => \YSquare\CategoryPath\Application\Model\CategoryList::class
    ],
    'controllers' => [
    ],
    'events'      => [
        'onActivate'   => '\YSquare\CategoryPath\Core\Events::onActivate',
        'onDeactivate' => '\YSquare\CategoryPath\Core\Events::onDeactivate',
    ],
    'templates'   => [
    ],
    'blocks'      => [
    ],
    'settings'    => [
    ]
];