# Y-Square Category Path Modul

## Installation
1. Erstellen Sie den Verzeichnispfad "packages/ysquare" in der selben Verzeichnisebene, in der die Ordner "source" und "vendor" liegen
2. Für das Modul erstellen Sie nun einen weiteren Unterordner "ys_category_path", in den die heruntergeladene Zip-Datei entpackt wird
3. Führen Sie anschließend folgende Befehle aus dem Hauptverzeichnis des Shopsystems aus:
   ```
   composer config repositories.ysquare '{"type":"path", "url": "./packages/ysquare/*"}'
   composer require --update-no-dev ysquare/ys_category_path
   ```
4. Nun kann das Modul im OXID eShop Admin aktiviert werden
5. Ggf. Tmp-Verzeichnis leeren und Views neu generieren

## Update
1. Entpacken Sie die neue Modulversion in den Ordner "packages/ysquare/ys_category_path"
2. Deaktivieren Sie das Modul im OXID eShop Admin
3. Führen Sie anschließend folgende Befehle aus dem Hauptverzeichnis des Shopsystems aus:
   ```
   composer update --no-dev ysquare/ys_category_path
   ```
   Die Frage, ob die Dateien des Pakets überschrieben werden sollen, bestätigen Sie bitte mit "Y".
4. Aktivieren Sie das Modul im OXID eShop Admin
5. Ggf. Tmp-Verzeichnis leeren und Views neu generieren
